The ITC_submission folder presents 3 solutions to the classification problem.
Here we try to explain what you will find in this folder, what kind of processing has been applied to the datas, what kind of models have been implemented and what results did they produce. 

1. Files
The data_utils.py file is the code to run to  process the datas, located in the data foler. Processed data are dumped into a dataset_[processingtype].pkl file that is  loaded by each model.
Each model generates 2 files, a modelfile in the models folder. This is the saved trained model that one can load direcltly to test the model of fine tune it. A prediction file which is the answer to the classification problem, located in pred folder. Both of the two neural network models also generate plot file in fig folder of the training/validation loss/accuracy to monitor and performances on both training and validation set aswell as the overfitting.
A tool file has been created not to implement useful functions as one-hot-conversion many times, and is imported in each model file.

2. Processing of the data
We applied to types of processing. The 1st one converts each qualitative variables of n categories into n-1 dummy (0-1) variables. The resulting dataset is a very sparse dataset that present the advantage of having most of its columns normed to 1, one of the drawbacks of this approach is that it adds a lot of variables, initially 29, to more than 300. The second one is based on a different idea for the processing of multicategories qualitative variables. Each variables are transformed into one variable taking its values in {1, .. , n_categories}. The drawback this approach has is the generation of unnormed variables, with different weights that has no particular sense. Say for example mod_1 = 1, mod_2 = 2, ... mod_2 = mean(mod_1, mod_3) has no particular mathematic sense.

3. Models
- The first model we investigated is a boosted tree with XGBoost, a very popular classifier in data challenges. We implemented a kind of hand-made cross validation to tune its parameters, lefting over a validation set, training the model on the remaining part, 3 times, evaluating on the validation then averaging over the 3 epochs of validation. This model results in the best validation accuracy.
- Neural network models: 2 bottleneck architecture networks has been implemented, one for each type of processing. The bottleneck architecture consists of sequentially stacking deacreasing-size layers in order to build a representation of lower dimmension as we go deeper, and finally output a distribution of probability over the 8 classes. 
As neural network models can overfit the datas, we implemented Dropout layers after the first Linear layers (tests had proven that the results are worse if we implement dropout after each Linear layers). We also implemented early stopping, to prevent the model to force the prediction to overfit the training set while the validation accuracy doesn't increase. 
Parameters like dropout, number of layers, patience in early stopping had been tested to find acceptable values.

4. Results
On the leftover validation set the boosted tree obtains better results than neural networks, reaching 85% of validation accuracy. The multimodel neural network model suffers from a lack of mathematic sense of its variables' realisations which make it reach only 65% of validation accuracy. The processing of qualitative variables as 0-1 dummies enables the to model to find more sense in variables' realisation and build features of higher quality for the classification reaching 76% of validation accuracy.
We expected xgboost to produce better results as there is not a lot of training example, which is required for neural networks to outperform other types of classifiers in supervised classification tasks.
